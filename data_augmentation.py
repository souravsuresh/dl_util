# -*- coding: utf-8 -*-
"""
**Data augmentation using skimage library**
"""

# %matplotlib inline
import matplotlib.pyplot as plt
import numpy as np
from skimage import data
from skimage.transform import rescale
from skimage.util import random_noise
from skimage.color import rgb2gray
from skimage import util
from skimage.transform import rotate
from skimage import exposure
from scipy import ndimage
from skimage import io
  
print("This is the data augmentation interface")
print('''
default_image <variable>
show_image(img_array)
rescale_image(img_array,rescale_factor)
add_random_noise(img_array)
convert_rgb2gray(image_array)
invert_img_color(img_array)
rotate_image(img_Array,angle)
change_contrast(img_array)
gamma_correction(img_array)
sigmoid_correction(img_array)
log_correction(img_array)
flip_horizontal(img_array)
flip_vertical(img_array)
blur_image(img_array)
read_image(img_path)
'''
)


default_image = data.chelsea()

def read_image(img_path):
    img = io.imread(img_path, as_grey=True)
    return img
    
def show_images(img_array):
    fig, axes = plt.subplots(nrows=1, ncols=2)
    ax = axes.ravel()
    ax[0].imshow(img_array, cmap='gray')
    ax[0].axis('off')
    plt.tight_layout()
       

"""**Rescaling**"""

def rescale_image(img_array,rescale_factor=0.25):
  # rescale image to 25% of the initial size by default
  image_rescaled = rescale(original_image, .25)
  return image_rescaled

"""**Add Random Noise**"""


def add_random_noise(image_array):
  image_with_random_noise = random_noise(image_array)
  return image_with_random_noise

"""**Convert to grayscale**"""

def convert_rgb2gray(image_array):
  gray_scale_image = rgb2gray(image_array)
  return gray_scale_image

"""**Image color inversion**"""


def invert_img_color(image_array):
  color_inversion_image = util.invert(image_array)
  return color_inversion_image

"""**Rotate Image**"""


def rotate_image(image_array,angle=45):
  # perform a 45 degree rotation by default
  image_with_rotation = rotate(image_array, angle)
  return image_with_rotation

"""**Change Contrast**"""



def change_contrast(image_array):
  v_min, v_max = np.percentile(original_image, (0.2, 99.8))
  better_contrast = exposure.rescale_intensity(image_array, in_range=(v_min, v_max))
  return better_contrast

"""**Gamma Correction**"""

# gamma and gain parameters are between 0 and 1

def gamma_correction(image_array):
  adjusted_gamma_image = exposure.adjust_gamma(image_array, gamma=0.4, gain=0.9)
  return adjusted_gamma_image

"""**Sigmoid Correction**"""

def sigmoid_correction(image_array):
  sigmoid_correction_image = exposure.adjust_sigmoid(original_image)
  return sigmoid_correction_image

"""**Logarathimic Correction**"""

def log_correction(image_array):
  log_correction_image = exposure.adjust_log(original_image)
  return log_correction_image

"""**Horizontal Flip**"""

def flip_horizontal(image_array):
  horizontal_flip = image_array[:, ::-1]
  return horizontal_flip

"""**Vertical Flip**"""

def flip_vertical(image_array):
  vertical_flip = image_array[::-1, :]
  return vertical_flip

"""**Blur Image**"""

def blur_image(image_array):
  blured_image = ndimage.uniform_filter(image_array, size=(11, 11, 1))
  return blured_image
